<?php require 'assets/connexion/racine.php'; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
	<head>
		<title>SDIS 29</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link href="<?php echo $racine; ?>/assets/CSS/style.css" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" href="images/logosdis29.gif" type="image/png" />
	</head>
	<body>
		<div id="container">
			<img class="logog" src="images/logo2.jpg" alt="Logo" width="91%"  height="72px"/>
			<img class="logod" src="images/logosdis29.gif" alt="Logo"  width="60px"  height="72px" />
			<div id="banner">
				<img src="images/main-image.jpg" alt="Main Image" />
			</div>
			<div id="nav">
				<div id="nav">
					<?php include 'assets/affichages/menu.php'; ?>
				</div>
			</div>
			<div id="content">
				<div id="left">
					<span class="flt-lft"><img src="images/Concarneau.jpg" alt="Thumbnail" /></span>
					<p class="date">
						Concarneau. Un exercice de grande ampleur dans lâemprise portuaire 
					</p>
					<p>
						Un important exercice simulant un incendie Ã  bord dâune vedette transportant des passagers sâest dÃ©roulÃ©, vendredi 13 avril, dans lâemprise portuaire Ã  Concarneau.
						
						Il est 20h00, le commandant de lâAZENOR lance un appel de dÃ©tresse et signale Ã  son bord un dÃ©but dâincendie alors que le navire entre dans le port. Parmi les nombreux passagers, le navire transporte aussi des enfants scolarisÃ©s sur la commune, partis en excursion aux Ã®les Les GlÃ©nan. Dans un mouvement de panique, certains occupants se jettent Ã  lâeau.
						Tel est le scÃ©nario imaginÃ© pour cet exercice organisÃ© par le groupement territorial de Concarneau. Le choix dâune thÃ©matique en milieu portuaire nâest pas le fruit du hasard : la ville accueillait, une semaine plus tard, le dÃ©part de la transat AG2R-La Mondiale.
						
						Par ailleurs, lâexercice constituait aussi une prÃ©paration Ã  la saison estivale et touristique, propice aux excursions et activitÃ©s nautiques dans notre dÃ©partement.
						
						Lâexercice grandeur nature a mobilisÃ© 94 sapeurs-pompiers issus de 19 centres dâincendie et de secours ainsi que la SNSM, la gendarmerie maritime, la capitainerie du port de pÃªche, la ville de Concarneau, la Chambre de Commerce et dâIndustrie MÃ©tropolitaine Bretagne Ouest (CCIMBO) et la compagnie CroisiÃ¨res Bleues, propriÃ©taire de lâAZENOR.
						
						Pour donner une dimension plus rÃ©aliste Ã  lâexercice, 47 Jeunes Sapeurs-Pompiers (JSP) des 3 sections du groupement territorial de Concarneau ont jouÃ©, pour lâoccasion, le rÃ´le des victimes.
						
						Lâexercice avait pour objectif principal de dÃ©cliner en interne les dispositions du plan ORSEC-SNV : Organisation de la RÃ©ponse de la SEcuritÃ© Civile â Secours Ã  Nombreuses Victimes.
						
						Lâexercice rÃ©pondait Ã©galement Ã  quatre autres niveaux dâobjectifs :
						<ul>
							<li>SâentraÃ®ner en inter services en prenant en compte les rÃ¨gles dâintervention spÃ©cifiques Ã  ce plan de secours.</li>
							<li>Mettre en Åuvre le nouveau VÃ©hicule de Poste de Commandement (VPC) de Bannalec et le Poste MÃ©dical AvancÃ© (PMA) de Quimper.</li>
							<li>Tester le dispositif SINUS qui permet de suivre les victimes en temps rÃ©el par lâapposition dâun bracelet dotÃ© dâun code barre et dâune fiche mÃ©dicale de lâavant.</li>
							<li>Mettre en Åuvre la chaÃ®ne mÃ©dicale du groupement SantÃ© impliquant lâintervention des mÃ©decins et infirmiers sapeurs-pompiers.</li>
						</ul>
						Â« Lâexercice a suscitÃ© une forte mobilisation parmi les personnels du SDIS 29 Â» a soulignÃ© le Lieutenant-colonel CÃ©dric BOUSSIN, chef du groupement territorial de Concarneau. Â« Il a permis de mettre en Åuvre les diffÃ©rents volets du dispositif ORSEC-SNV. La coopÃ©ration entre services en termes dâorganisation et de communication a Ã©tÃ© efficace Â».
					</p>
					<p class="flt-rgt">
						
					</p>
				</div>
				<div id="right">
					<div id="headlines">
						<span class="heading1">Actu</span>
						<p class="date">
							Secours Ã  personne. Un accord trouvÃ© entre les SDIS bretons et lâARS Bretagne
						</p>
						<p>
							Les 4 PrÃ©sidents de Conseil dâadministration des SDIS bretons : Gilles DUFEIGNEUX -Morbihan, Nicole ZIEGLER â FinistÃ¨re, Jean-Luc CHENUT â Ille-et-Vilaine et Yannick MORIN 1er vice-prÃ©sident â CÃ´tes dâArmor se sont rendus, le 6 avril 2018, Ã  lâAgence rÃ©gionale de la SantÃ© (ARS), pour faire suite Ã  la rÃ©union dâavril 2017 afin de discuter de lâÃ©volution de la distribution des secours Ã  la personne sur le territoire rÃ©gional. Leur objectif : simplifier et clarifier lâorganisation et les compÃ©tences des diffÃ©rents acteurs afin de faire Ã©voluer, dans le bon sens, le secours Ã  personne en Bretagne.
						</p>
						<p class="date">
							ANTARES. Le dÃ©ploiement se poursuit
						</p>
						<p>
							Tue Dec 2 04:00 PM 
							<br />
							Le SDIS 29 poursuit lâinstallation de son nouveau systÃ¨me de transmissions : ANTARES, acronyme dâAdaptation Nationale des Transmissions Aux Risques Et aux Secours.
						</p>
						<span class="flt-rgt"><a href="http://www.free-css.com/">MORE</a></span>
					</div>
					<div id="stats">
						<span class="heading2">L'agenda du SDIS 29</span>
						<table>
							<tr>
								<td>
									16-06-2018
								</td>
								<td colspan="2">
									JournÃ©e Nationale des Sapeurs Pompiers.
								</td>
							</tr>
							<tr>
								<td>
									26-05-2018
								</td>
								<td>
									PSSP zonal
								</td>
							</tr>
							<tr>
								<td>
									24-05-2018
								</td>
								<td>
									CÃ©rÃ©monie de promotion de grade et de remise de mÃ©daille
								</td>
							</tr>
						</table>
					</div>
				</div>
				<div style="clear:both"></div>
			</div>
			<div id="footer">
				<?php include 'assets/affichages/footer.php'; ?>
			</div>
		</div>
	</body>
</html>

